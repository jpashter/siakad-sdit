<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Tentang extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'idTentang'   => [
					'type'           => 'INT',
					'constraint'     => 5,
					'unsigned'       => true,
					'auto_increment' => true,
			],
			'tentang'  => [
					'type'       => 'VARCHAR',
					'constraint' => '100',
					'null' 		=>True,
			],
			'isiTentang' => [
					'type'       => 'text',
					'null' 		=>True,
			],
			'keterangan' => [
					'type'       => 'text',
					'null' 		=>True,
			],
			'slug' => [
					'type'       => 'text',
					'null' 		=>True,
			],
			
			'created_at' => [
					'type' => 'DATE',
			],
			'updated_at' => [
					'type' => 'DATE',
			],
			
		]);

		$this->forge->addKey('idTentang', true);
		$this->forge->createTable('tentang');
	}

	public function down()
	{
		$this->forge->dropTable('tentang');
	}
}
