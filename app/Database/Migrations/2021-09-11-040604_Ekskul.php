<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Ekskul extends Migration
{

	public function up()
	{
		$this->forge->addField([
			'idEkskul'   => [
					'type'           => 'INT',
					'constraint'     => 5,
					'unsigned'       => true,
					'auto_increment' => true,
			],
			'ekskul'  => [
					'type'       => 'VARCHAR',
					'constraint' => '100',
					'null' 		=>True,
			],
			'keterangan' => [
					'type'       => 'text',
					'null' 		=>True,
			],
			
			'created_at' => [
					'type' => 'DATE',
			],
			'updated_at' => [
					'type' => 'DATE',
			],
			
		]);

		$this->forge->addKey('idEkskul', true);
		$this->forge->createTable('ekskul');
	}

	public function down()
	{
		$this->forge->dropTable('ekskul');
	}
}
