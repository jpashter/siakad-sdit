<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Fasilitas extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'idFas'   => [
					'type'           => 'INT',
					'constraint'     => 5,
					'unsigned'       => true,
					'auto_increment' => true,
			],
			'fasilitas'  => [
					'type'       => 'VARCHAR',
					'constraint' => '150',
					'null' 		=>True,
			],
			'keterangan' => [
					'type'       => 'text',
					'null' 		=>True,
			],
			
			'created_at' => [
					'type' => 'DATE',
			],
			'updated_at' => [
					'type' => 'DATE',
			],
			
		]);

		$this->forge->addKey('idFas', true);
		$this->forge->createTable('fasilitas');
	}

	public function down()
	{
		$this->forge->dropTable('fasilitas');
	}
}
