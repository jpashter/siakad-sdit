<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Berita extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'idBerita'   => [
					'type'           => 'INT',
					'constraint'     => 5,
					'unsigned'       => true,
					'auto_increment' => true,
			],
			'judulBerita'  => [
					'type'       => 'VARCHAR',
					'constraint' => '60',
					'null' 		=>True,
			],
			'slug'  => [
					'type'       => 'VARCHAR',
					'constraint' => '60',
					'null' 		=>True,
			],
			'isiBerita'  => [
					'type'       => 'text',
			],
            'keterangan' =>[
                    'type' => 'varchar',
                    'constraint' => '25'
            ],
            'tag' => [
                'type' => 'text'
            ],
			'created_at' => [
					'type' => 'DATE',
			],
			'updated_at' => [
					'type' => 'DATE',
			],

			
			
		]);

		$this->forge->addKey('idBerita', true);
		$this->forge->createTable('berita');
	}

	public function down()
	{
		$this->forge->dropTable('berita');
	}
}
