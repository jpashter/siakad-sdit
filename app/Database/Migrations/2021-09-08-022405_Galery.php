<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Galery extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'idGambar'   => [
					'type'           => 'INT',
					'constraint'     => 5,
					'unsigned'       => true,
					'auto_increment' => true,
			],
			'namaGambar'  => [
					'type'       => 'VARCHAR',
					'constraint' => '50',
					'null' 		=>True,
			],
			'caption' => [
					'type'       => 'VARCHAR',
					'constraint' => '100',
					'null' 		=>True,
			],

			'keterangan' => [
					'type'       => 'VARCHAR',
					'constraint' => '50',
					'null' 		=>True,
			],
			
			'created_at' => [
					'type' => 'DATE',
			],
			'updated_at' => [
					'type' => 'DATE',
			],
			
		]);

		$this->forge->addKey('idGambar', true);
		$this->forge->createTable('galery');
	}

	public function down()
	{
		$this->forge->dropTable('galery');
	}
}
