<?= $this->extend('templates/layout/userlayout'); ?>

<?= $this->section('content') ?>

<div class="container mt-5">
    <div class="row">
        <div class="col-md-4 position-relative">
            <div class="position-sticky  bg-white p-4">
                <h3 class='title'>Detail Sekolah</h3>
                <div class="garis bg-success rounded-pill mb-5 "></div>
                <div class="card">
                    <div class="card-body mt-2">
                        <div class="mx-auto" style="width:200px">
                            <img width="100%" src="/assets/img/<?= $prof['logo'] ?>" alt="">
                        </div>
                    </div>
                </div>
                <div  style="font-family:ubuntu">
                    <div class="form-group">
                        <strong><i class="bi bi-building"></i> Nama Sekolah</strong>
                        <div><?= $prof['namasekolah'] ?></div>
                        <hr>
                    </div>
                    <div class="form-group">
                        <strong><i class="bi bi-geo-alt"></i> Alamat Sekolah</strong>
                        <div><?= $alamat ?></div>
                        <hr>
                    </div>
                    <div class="form-group">
                        <strong><i class="bi bi-geo-alt"></i>No Pokok Sekolah Nasional</strong>
                        <div><?= $prof['npsn'] ?></div>
                        <hr>
                    </div>
                    <div class="form-group">
                        <strong><i class="bi bi-geo-alt"></i>No Induk Sekolah</strong>
                        <div><?= $prof['nis'] ?></div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7 mb-5 pt-3 pt-md-0">
            <div class="container-fluid bg-white p-4">
                <h3 class="title text-center p-4">Profil Sekolah</h3>
                <div class="garis bg-light mx-auto rounded-pill mb-5 "></div>
                <div class="row hg-100">
                    <div class="timeline" >
                        <div class="timelinebox right"  data-aos="fade-right" data-aos-duration="1000">
                            <div class="content py-2 px-3">
                                <h4  >Extra Kurikuler</h4>
                                <ul>
                                    <li>Pramuka</li>
                                    <li>Palang Merah</li>
                                    <li>Pencak Silat</li>
                                </ul>
                            </div>
                        </div>
                        <div class="timelinebox right"  data-aos="fade-right" data-aos-duration="1000">
                            <div class="content mt-3 py-2 px-3" >
                                <h4 >Program Sore</h4>
                                <ul>
                                    <li>Tahfiz Qur'an</li>
                                    <li>English Habit</li>
                                    <li>Kreatifitas Siswa</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
            
        </div>
        
    </div>
   
</div>
<script>
  $(".thumb-box").mouseover(function(){
    var id = $(this).data('id');
    // var detail = document.getElementById("box_"+id);
    $( "#box_"+id ).show( "fold", 200 );
    // detail.classList.add("d-none");
  })
  $(".thumb-box").mouseleave(function(){
    var id = $(this).data('id');
    // var detail = document.getElementById("box_"+id);
    $( "#box_"+id ).hide( "fold", 200 );
    // detail.classList.add("d-none");
  })
</script>
<?= $this->endsection(); ?>