<?= $this->extend('templates/layout/userlayout'); ?>

<?= $this->section('content') ?>

<div class=" container mt-5">
    
    <div class="row">
        <div class="col-md-8 mb-5">
            <div class="bg-white p-3">
                <div class="row">
                    <?php foreach($galeries as $galery) : ?>
                    <div class="col-6 col-md-4">
                        <div class="slide-box overflow-hidden w-100">
                            <a href="/assets/galery/<?= $galery['namaGambar'] ?>" data-lightbox="image-1" data-title="<?= $galery['caption']?>" class="title">
                            <img onload="cek(<?= $galery['idGambar'] ?>)" id="<?= $galery['idGambar'] ?>"  src="/assets/galery/<?= $galery['namaGambar'] ?>" >
                            </a>
                        </div>
                        <div class="caption mb-2">
                            <span><?= $galery['caption']?></span>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <?= $pager->links('galery','berita_pagination'); ?>
            </div>
        </div>
        <div class="col-md-4 position-relative">
            <div class="position-sticky  bg-white p-md-4">
                <?= $this->include("templates/content/popular") ?>
            </div>
        </div>
    </div>
   
</div>
<script>
      lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true,
      'positionFromTop' : 100,
    })

    function cek(id){
        var image = document.getElementById(id);
        var height = image.offsetHeight;
        var width = image.offsetWidth;

        if (height == width){
        image.style.width = "100%";
        }else if (height < width ){
        image.style.height = "100%";
        }else{
        image.style.width = "100%";
        }
    }
    
</script>

<?= $this->endSection() ?>