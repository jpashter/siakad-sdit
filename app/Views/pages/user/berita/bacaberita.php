<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $judul ?></title>
    <meta property="og:url" content="<?=  $url ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:title"  content="<?= $berita['judulBerita'] ?>" />
    <meta property="og:description" content="<?= $berita['slug'] ?>..." />
    <meta property="og:image"  content="<?= site_url()."assets/thumbnailberita/$berita[thumbnail]" ?>" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Fjalla+One&family=Quicksand:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Oswald&family=Rubik&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <link href="/assets/css/bootstrap.beta3.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/bold.css">
    <link rel="stylesheet" href="/assets/css/perfect-scrollbar.css">
    <link rel="stylesheet" href="/assets/css/bootstrap-icons.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <link rel="stylesheet" href="/assets/css/userstyle.css">
    <link rel="stylesheet" href="/assets/aos-master/dist/aos.css">
    <!-- <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet"> -->
    <link href="/assets/glightbox/css/glightbox.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script src="/assets/js/jquery-ui/jquery-ui.min.js"></script>
    <!-- <link rel="shortcut icon" href="https://technext.github.io/mazer/assets/images/favicon.svg" type="image/x-icon"> -->
    <style>
        .fileUpload {
     position: relative;
     overflow: hidden;
     margin: 10px;
     width:150px;
     height:150px;
 }
 .fileUpload img{
    width:100%;min-height:100%;
 }

 input.upload {
     position: absolute;
     top: 0;
     right: 0;
     margin: 0;
     padding: 0;
     font-size: 20px;
     cursor: pointer;
     opacity: 0;
     filter: alpha(opacity=0);
 }
    </style>
</head>

<body style="font-family:Quicksand">
<?= $this->include("templates/components/usertopbar") ?>




<div class=" container mt-5">
    
    <div class="row">
        <div class="col-md-1">
            <span class="fs-1 start-1 bottom-0 end-0 top-50  translate-middle-y  sharelink">
                <a href="whatsapp://send?text=<?= $url ?>" class="bg-success share-button rounded px-2 text-light">
                    <i class="bi bi-whatsapp"></i>
                </a><br>
                <a href="https://plus.google.com/share?url=<?= $url ?>"  class="bg-purple px-2 share-button rounded text-light">
                    <i class="bi bi-google"></i>
                </a><br>
                <a href="http://www.facebook.com/sharer.php?u=<?= $url ?>"  class="bg-primary px-2 share-button rounded text-light">
                    <i class="bi bi-facebook"></i>
                </a><br>
                <a href="https://twitter.com/share?url=<?= $url ?>"  class="bg-info px-2 share-button rounded text-light">
                    <i class="bi bi-twitter"></i>
                </a><br>
                <a  class="bg-info px-2 d-md-none share-triger rounded text-light">
                    <i class="share-icon bi bi-share-fill"></i>
                </a><br>

            </span>
        </div>
        <div class="col-md-7 mb-5">
            <div class="p-3 p-md-5 bg-white">
                <div class="read-thumbnail">
                    <img class="w-100 rounded" src="/assets/thumbnailberita/<?= $berita['thumbnail'] ?>" alt="">
                </div>
                <div class="judul fs-2">
                    <strong><?= $berita['judulBerita'] ?></strong>
                </div>
                <div>
                    <small>
                        <i class="bi bi-person"></i>
                        <?= $berita['penulis'] ?> ||
                        <i class="bi bi-calendar"></i>
                        <?= $tglpublish ?> ||
                        <i class="bi bi-clock-history"></i>
                        <?=  $waktupublish ?> 
                    </small>
                </div>
                <hr>
                <p style="font-family:Arial" >
                    <?= $berita['isiBerita'] ?>
                </p>
            </div>
        </div>
        <div class="col-md-4 position-relative">
            <div class="position-sticky  bg-white p-md-4">
                <?= $this->include("templates/content/popular") ?>
            </div>
        </div>
    </div>
   
</div>
<script>
    $(".share-triger").click(function(){
        $(".share-button").fadeToggle("slow");
    })
</script>
<?= $this->include("templates/components/usermodal") ?>
<?= $this->include("templates/components/userfooter") ?>