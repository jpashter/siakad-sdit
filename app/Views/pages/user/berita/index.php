<?= $this->extend('templates/layout/userlayout'); ?>

<?= $this->section('content') ?>

<div class="container mb-4 mt-5 min-hg-100">
        
    <div class="row">
        <div class="col-md-8 bg-white">
            <!-- very new artikel -->
            <?php  if($allnews == null ): ?>
                <di class="container-fluid">
                    <h3 class="text-center py-5">Tidak Ada Berita untuk Ditampilkan</h3>
                </di> 
            <?php else: ?>  
            <div class="row headnews">
                <div class="col-md-4 position-relative">
                    <span class="title fs-3 position-absolute top-50 start-50 translate-middle hotnews-flag">
                        HOT <strong>NEWS</strong>
                    </span>
                </div>
                <div class="col-md-8 p-0">
                    <div class=" p-4">
                        <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-indicators  ">
                                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                            </div>
                            <div class="carousel-inner border border-secondary rounded">
                                <div class="carousel-item carousel-box  active">
                                    <img src="/assets/thumbnailberita/<?= $terbaru['thumbnail'] ?>" class="d-block w-100" alt="...">
                                    <div class="carousel-caption carouse-capt position-absolute top-0 start-0 bg-alpha">
                                        <div class="text p-5 postion-absolute bottom-0">
                                            <h5 class='title'><?= $terbaru['judulBerita'] ?></h5>
                                            <p>Penulis : <?= $terbaru['penulis'] ?></p>
                                        </div>
                                    </div>
                                </div>
                                <?php foreach($slide as $b) : ?>
                                <div class="carousel-item carousel-box ">
                                    <img src="/assets/thumbnailberita/<?= $b['thumbnail'] ?>" class="d-block w-100" alt="...">
                                    <div class="carousel-caption position-absolute top-0 start-0  carouse-capt bg-alpha">
                                        <div class="text p-5 postion-absolute bottom-0">
                                            <h5 class='title'>Second slide label</h5>
                                            <p>Penulis : <?= $terbaru['penulis'] ?></p>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                                <!-- <div class="carousel-item">
                                    <img src="..." class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Third slide label</h5>
                                        <p>Some representative placeholder content for the third slide.</p>
                                    </div>
                                </div> -->
                            </div>
                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="title my-4">
                <h3 class="text-center">Daftar Berita</h3>
                <div class="garis mx-auto bg-purple"></div>
            </div>
            <?php  if($allnews == null ): ?>
            <di class="container-fluid">
                <h3 class="text-center py-5">Tidak Ada Berita untuk Ditampilkan</h3>
            </di> 
            <?php endif; ?>                     
            <div class="container-fluid row mt-3">
                
                <?php foreach($allnews as $news ): ?>
                    <div class="col-6 p-1 p-md-3">
                        <a href="/info/baca/<?= $news['slug'] ?>" class="native-link ">
                        <div class="card news-card" >
                            <div class="div thumbnail-news">
                                <img src="/assets/thumbnailberita/<?= $news['thumbnail'] ?>" class="card-img-top w-100" alt="...">
                            </div>
                            <div class="caption pt-3">
                                <h5 class="title caption"><?= $news['judulBerita'] ?></h5>
                            </div>

                        </div>
                        </a>
                    </div>
                <?php endforeach;?>
            </div>
            <?= $pager->links('berita','berita_pagination') ?>

            <?php endif;?>
        </div>
        <div class="col-md-4">
            <div class="position-sticky right-item top-0 end-0 mt-3 mt-md-0 bg-white p-4">
                <?= $this->include("templates/content/popular") ?>
            </div>
        </div>
    </div>
</div>
<?php $this->endsection(); ?>