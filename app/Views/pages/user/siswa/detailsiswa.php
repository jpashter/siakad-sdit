<?= $this->extend('templates/layout/userlayout'); ?>

<?= $this->section('content') ?>

<div class="container mt-5">
    <div class="row">
        <div class="col-md-8 mb-5">
            <div class="p-3 p-md-5 bg-white">

                <h3 class="text-center title">DATA SISWA</h3>
                <div class="garis bg-success mx-auto rounded-pill mb-5 "></div>
                <!-- <hr> -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="container-fluid mb-3">
                            <img class="w-100 border border-1 border-success shadow" src="/assets/photosiswa/<?= $siswa['photo'] ?>" alt="" srcset="">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div  style="font-family:ubuntu">
                            <h4><?= ucwords($siswa['namasiswa'])?></h4>
                            <!-- <hr> -->
                            <div class="text-secondary">

                                <small>Nis : <?= $siswa['nis'] ?></small> 
                            </div>
                            <hr>
                        </div>
                        
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Jenis Kelamin</strong>
                                <span class="position-absolute end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $siswa['kelamin'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Alamat</strong>
                                <span class="position-absolute end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $siswa['alamat'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Tanggal Lahir</strong>
                                <span class="position-absolute  end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $siswa['tgllahir'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Nama Orang Tua</strong>
                                <span class="position-absolute top-0 end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <strong> Ayah</strong> <br>
                                <?= $siswa['namaAyah'] ?> <br>
                                <strong> Ibu</strong> <br>
                                <?= $siswa['namaIbu'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Pekerjaan Orang Tua</strong>
                                <span class="position-absolute top-0 end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <strong> Ayah</strong> <br>
                                <?= $siswa['pekAyah'] ?> <br>
                                <strong> Ibu</strong> <br>
                                <?= $siswa['pekIbu'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Alamat Orang Tua</strong>
                                <span class="position-absolute top-0 end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <strong> Ayah</strong> <br>
                                <?= $siswa['alamatAyah'] ?> <br>
                                <strong> Ibu</strong> <br>
                                <?= $siswa['alamatIbu'] ?>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 position-relative">
            <?= $this->include("templates/content/popular") ?>
        </div>
    </div>
   
</div>
<?= $this->endsection(); ?>