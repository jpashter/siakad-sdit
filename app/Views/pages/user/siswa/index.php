<?= $this->extend('templates/layout/userlayout'); ?>

<?= $this->section('content') ?>

<div class="container mt-5">
    <div class="row">
        <!-- <div class="col-md-4 position-relative">
            <div class="position-sticky  bg-white p-4">
                <h3 class='title'>Pejabat Sekolah</h3>
                <div class="garis bg-success rounded-pill mb-5 "></div>

            </div>
        </div> -->
        <div class="col-md-12 mb-5 pt-3 pt-md-0">
            <div class="container-fluid bg-white p-4">
                <h3 class='fm-ubuntu text-center'>Siswa</h3>
                <hr>
                <div class="row">
                    <?php foreach($semuasiswa as $siswa) :?>
                        <div class="col-md-4 col-12 p-2">
                            <a href="/info/siswa/<?= $siswa['namasiswa'] ?>" class="native-link">
                           <div class="row card-siswa py-2 m-2 border">
                                <div class="col-5 ">
                                    <div class="img-siswa-box overflow-hidden position-relative">
                                        <img src="/assets/photosiswa/<?= $siswa['photo'] ?>" class="thumbnail translate-middle position-absolute top-50 start-50" style="height:100%" alt="">
                                    </div>
                                </div>
                                <div class="col-7 position-relative">
                                    <strong class="title fs-3"><?= ucwords($siswa['namasiswa']) ?></strong>
                                    <span class="fm-ubuntu position-absolute bottom-0 ms-2 start-0"><small><i class="bi bi-calendar-event-fill"></i> <?= $siswa['tgllahir'] ?></small></span>
                                </div>
                           </div>
                           </a>
                        </div>
                    <?php endforeach; ?> 
                </div>


            </div>
            <div class="container-fluid bg-white p-4">
                <h3 class='title text-center'>Staf dan Tata Usaha</h3>
                <div class="garis bg-success rounded-pill mb-5  mx-auto"></div>

                <div class="row">
                     
                </div>


            </div>
        </div>
        
    </div>
   
</div>
<script>
  $(".thumb-box").mouseover(function(){
    var id = $(this).data('id');
    // var detail = document.getElementById("box_"+id);
    $( "#box_"+id ).show( "fold", 200 );
    // detail.classList.add("d-none");
  })
  $(".thumb-box").mouseleave(function(){
    var id = $(this).data('id');
    // var detail = document.getElementById("box_"+id);
    $( "#box_"+id ).hide( "fold", 200 );
    // detail.classList.add("d-none");
  })
</script>
<?= $this->endsection(); ?>