<?= $this->extend('templates/layout/userlayout'); ?>

<?= $this->section('content') ?>

<div class="container mt-5">
    <div class="row">
        <div class="col-md-5 position-relative">
            <div class="position-sticky  bg-white p-4">
                <h3 class='title'>Pejabat Sekolah</h3>
                <div class="garis bg-success rounded-pill mb-5 "></div>

                <a href="/info/guru/<?= $kepsek['namaguru'] ?> " class="native-link">
                        
                    <div class="row">
                        <div class="col-4">
                            <div class="thumb text-center bg-white">
                                <img class="w-100 mx-auto" src="/assets/photoguru/<?= $kepsek['photo'] ?>" alt="">
                            </div>
                        </div>
                        <div class="col-8">
                            <div  style="font-family:ubuntu">
                                <h4><?= $kepsek['namaguru'].','.$kepsek['gelar'] ?></h4>
                                <!-- <hr> -->
                                <div class="text-secondary">
                                    <small><?= $kepsek['jabatan'] ?></small> <br>
                                    <small><i><?= "$kepsek[status] : $kepsek[niy]" ?></i></small>
                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>
                </a>
                <?php foreach($waka as $w) : ?>
                    <a href="/info/guru/<?= $w['namaguru'] ?> " class="native-link">
                        
                    <div class="row mt-3">
                        <div class="col-4">
                            <div class="thumb text-center bg-white">
                                <img class="w-100 mx-auto" src="/assets/photoguru/<?= $w['photo'] ?>" alt="">
                            </div>
                        </div>
                        <div class="col-8">
                            <div  style="font-family:ubuntu">
                                <h4><?= $w['namaguru'].','.$w['gelar'] ?></h4>
                                <!-- <hr> -->
                                <div class="text-secondary">
                                    <small><?= $w['jabatan'] ?></small> <br>
                                    <small><i><?= "$w[status] : $w[niy]" ?></i></small>
                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>

                    </a>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-md-7 mb-5 pt-3 pt-md-0">
            <div class="container-fluid bg-white p-4">
                <h3 class='title text-center'>Guru Mata Pelajaran</h3>
                <div class="garis bg-success rounded-pill mb-5  mx-auto"></div>

                <div class="row">
                    <?php foreach($guru as $g) :?>
                        <div class="col-md-4 col-6">
                            <div  data-id="<?= $g['idGuru'] ?>"  class="thumb-box position-relative mx-auto">
                                <a href="/info/guru/<?= $g['namaguru'] ?>" class="fs-6 text-light">
                                    <div id="box_<?= $g['idGuru'] ?>" class="position-absolute  fade-box  detailbox hg-100 w-100" style="display:none" >
                                        <div class="text-center top-50 start-50 translate-middle position-absolute">
                                            <strong class="text-warning"> <i class="bi bi-filter-circle-fill"> </i> Details</strong>
                                        </div>
                                    </div>
                                </a>
                                <div class="thumbnail">
                                    <img class="w-100" src="/assets/photoguru/<?= $g['photo'] ?>" alt="" srcset="">
                                </div>
                                <div class="title text-center"><?= "$g[namaguru],$g[gelar] " ?></div>
                            </div>
                        </div>
                    <?php endforeach; ?>  
                </div>


            </div>
            <div class="container-fluid bg-white p-4">
                <h3 class='title text-center'>Staf dan Tata Usaha</h3>
                <div class="garis bg-success rounded-pill mb-5  mx-auto"></div>

                <div class="row">
                    <?php foreach($staf as $g) :?>
                        <div class="col-md-4 col-6">
                            <div  data-id="<?= $g['namaguru'] ?>"  class="thumb-box position-relative mx-auto">
                                <a href="/info/guru/<?= $g['namaguru'] ?>" class="fs-6 text-light">
                                    <div id="box_<?= $g['namaguru'] ?>" class="position-absolute  fade-box  detailbox hg-100 w-100" style="display:none" >
                                        <div class="text-center top-50 start-50 translate-middle position-absolute">
                                            <strong class="text-warning"> <i class="bi bi-filter-circle-fill"> </i> Details</strong>
                                        </div>
                                    </div>
                                </a>
                                <div class="thumbnail">
                                    <img class="w-100" src="/assets/photoguru/<?= $g['photo'] ?>" alt="" srcset="">
                                </div>
                                <div class="title text-center"><?= "$g[namaguru],$g[gelar]" ?></div>
                            </div>
                        </div>
                    <?php endforeach; ?>  
                </div>


            </div>
        </div>
        
    </div>
   
</div>
<script>
  $(".thumb-box").mouseover(function(){
    var id = $(this).data('id');
    // var detail = document.getElementById("box_"+id);
    $( "#box_"+id ).show( "fold", 200 );
    // detail.classList.add("d-none");
  })
  $(".thumb-box").mouseleave(function(){
    var id = $(this).data('id');
    // var detail = document.getElementById("box_"+id);
    $( "#box_"+id ).hide( "fold", 200 );
    // detail.classList.add("d-none");
  })
</script>
<?= $this->endsection(); ?>