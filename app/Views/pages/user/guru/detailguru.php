<?= $this->extend('templates/layout/userlayout'); ?>

<?= $this->section('content') ?>

<div class="container mt-5">
    <div class="row">
        <div class="col-md-8 mb-5">
            <div class="p-3 p-md-5 bg-white">

                <h3 class="text-center title">DATA GURU</h3>
                <div class="garis bg-success mx-auto rounded-pill mb-5 "></div>
                <!-- <hr> -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="container-fluid mb-3">
                            <img class="w-100 border border-1 border-success shadow" src="/assets/photoguru/<?= $guru['photo'] ?>" alt="" srcset="">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div  style="font-family:ubuntu">
                            <h4><?= $guru['namaguru'].','.$guru['gelar'] ?></h4>
                            <!-- <hr> -->
                            <div class="text-secondary">

                                <small><?= $guru['jabatan'] ?></small> |
                                <small><i><?= "$guru[status] : $guru[niy]" ?></i></small>
                            </div>
                            <hr>
                        </div>
                        
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Jenis Kelamin</strong>
                                <span class="position-absolute end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $guru['kelamin'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Alamat</strong>
                                <span class="position-absolute end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $guru['alamat'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Tanggal Lahir</strong>
                                <span class="position-absolute  end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $guru['tgllahir'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Pendidikan Terahir</strong>
                                <span class="position-absolute top-0 end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $guru['pendidikan'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Perguruan Tinggi</strong>
                                <span class="position-absolute top-0 end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $guru['pt'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Jurusan</strong>
                                <span class="position-absolute end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $guru['jurusan'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Email</strong>
                                <span class="position-absolute end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $guru['email'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Nomor HP/WA</strong>
                                <span class="position-absolute end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $guru['hp'] ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 position-relative">
            <?= $this->include("templates/content/popular") ?>
        </div>
    </div>
   
</div>
<?= $this->endsection(); ?>