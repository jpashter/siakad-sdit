<?= $this->extend('templates/layout/userlayout'); ?>

<?= $this->section('content') ?>
<script>
  // $( function() {
  //   // run the currently selected effect
  //   function runEffect() {
  //     // get effect type from
  //     var selectedEffect = $( "#effectTypes" ).val();
 
  //     // Most effect types need no options passed by default
  //     var options = {};
  //     // some effects have required parameters
  //     if ( selectedEffect === "scale" ) {
  //       options = { percent: 50 };
  //     } else if ( selectedEffect === "size" ) {
  //       options = { to: { width: 280, height: 185 } };
  //     }
 
  //     // Run the effect
  //     $( "#effect" ).show( selectedEffect, options, 500, callback );
  //   };
 
  //   //callback function to bring a hidden box back
  //   function callback() {
  //     setTimeout(function() {
  //       $( "#effect:visible" ).removeAttr( "style" ).fadeOut();
  //     }, 1000 );
  //   };
 
  //   // Set effect from select menu value
  //   $( "#button" ).on( "click", function() {
  //     runEffect();
  //   });
 
  //   $( "#effect" ).hide();
  // } );
</script>
<!-- <div class="hg-100  position-relative">
    <div id="carouselExampleControls" class="carousel slide myslide" data-bs-ride="carousel">
        <div class="carousel-inner ">
            <div class="carousel-item active">
                <img src="assets/slide/1.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="assets/slide/2.jpg" class="d-block  w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="assets/slide/3.jpg" class="d-block  w-100" alt="...">
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</div> -->
<?= $this->include('templates/components/loading'); ?>

<section id="hero" class="min-hg-100 softgreen text-dark">
    <div style="height:100%" class="hg-100 container">
        <div class="row">
            <div data-aos="fade-left" data-aos-duration="2000" class="col-md-6 text-center pt-5">
                <img src="/assets/img/<?= $profil['logo'] ?>" class="w-60 " alt=""><br>
                <!-- <span class="title fs-3 px-5 py-2 rounded-pill text-dark bg-warning">Marhaban Ya Ramadhan</span> -->
            </div>
            <div da ta-aos="fade-left" data-aos-duration="800"  class="col-md-6 p-0 p-md-5 text-end position-relative">
                <div class=" position-absolute pt-md-5 pt-2 mt-md-5 mt-2 w-80 text-md-end text-center">
                    <h2 class="hero-title mt-3"><?= $profil['namasekolah'] ?></h2>
                    <span class="hero-subtitle bg-warning px-3 py-2 rounded-pill shadow-sm">NW Sembalun Bumbung</span>
                    <p class="mt-3 ms-md-5 ps-md-5">Yayasan Pendidikan Said Alamin Nahdlatul Wathan Sembalun Bumbung    </p>
                    <hr>
                    <button data-bs-toggle="modal" data-bs-target="#daftar" class="btn mt-5 btn-success px-4"> <i class="bi bi-arrow-left-circle"></i>   
                        Daftar Sekarang
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="py-5 mb-5 bg-light">
    <div  class=" container">
        <div class="row justify-content-center">
            <div class="col-md-2">
                <div class="icon-box mx-auto mb-4">
                      <img src="/assets/icons/teacher.png" alt="" class="w-100 rounded-circle border border-5 shadow border-white">
                      <div class="caption fm-ubuntu fs-1 text-center">
                        <?= $jumguru ?>
                        <h5 class="text-center fs-5 fm-ubuntu">GURU</h5>
                      </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="icon-box mx-auto mb-4">
                    <img src="/assets/icons/student-color.png" alt="" class="w-100 rounded-circle border border-5 shadow border-white">
                    <div class="caption fm-ubuntu fs-1 text-center">
                        <?= $jumsiswa ?>
                        <h5 class="text-center fs-5 fm-ubuntu">PELAJAR</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="icon-box mx-auto mb-4">
                    <img src="/assets/icons/article.png" alt="" class="w-100 rounded-circle border border-5 shadow border-white">
                    <div class="caption fm-ubuntu fs-1 text-center">
                        <?= $jumberita ?>
                        <h5 class="text-center fs-5 fm-ubuntu">ARTIKEL</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- <section> -->

</section>
<section class="" id="kepsek">
    <div class="container">
        <h3 class="title text-center p-4" data-aos="zoom-out" data-aos-duration="1000">Sambutan Kepala Sekolah</h3>

        <div class="garis bg-success mx-auto rounded-pill mb-5 "></div>
        <div class="row">
            <div class="col-md-6">
                <div class="w-50 mx-auto">
                    <img data-aos="fade-left" data-aos-duration="1000" class="w-100" src="<?= $kepsek==null ? 'assets/img/maulanasyaikh.jpg' : '/assets/photoguru/'.$kepsek['photo'] ; ?>" alt="">
                </div>
            </div>
            <div class="col-md-6 pb-5">
                <p  data-aos="fade-right" data-aos-duration="1000">
                    <!-- text sambutan kepala sekolah  -->
                    
                   <?= $sambutan == null ? 'Belum Ada Kata Sambutan' : substr($sambutan['isiTentang'],0,500) ; ?>
                </p>
                <button class="btn btn-primary btn-lg shadow">Baca Selengkapnya <i class="bi bi-arrow-right"></i> </button>
            </div>
        </div>

    </div>
</section>

<!-- profl sekolah  -->
<section class="bg-darkgreen overflow-hidden text-softwarning min-hg-80">
    <div class="container hg-100">
        <h3 class="title text-center p-4">Profil Sekolah</h3>
        <div class="garis bg-light mx-auto rounded-pill mb-5 "></div>
        <div class="row hg-100">
            <div class="col-md-4 ps-md-5 min-hg-100 ">
                <div class="puzzle-image w-100 d-none d-md-block" data-aos="zoom-out" data-aos-duration="2000" >
                    <div  class="img-tentang-box ms-5 position-absolute img-1">
                        <img class="w-100 border-3 border border-light " src="assets/santri/1.jpg" alt="">
                    </div>
                    <div  class="img-tentang-box position-absolute img-2">
                        <img class="w-100 border-3 border border-light " src="assets/santri/2.jpg" alt="">
                    </div>
                    <div  class="img-tentang-box position-absolute img-3">
                        <img class="w-100 border-3 border border-light " src="assets/santri/3.jpeg" alt="">
                    </div>
                </div>
                <div class="puzzle-image w-100 d-md-none mb-3">
                    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item carousel-box active">
                              <img src="assets/santri/1.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item carousel-box">
                              <img src="assets/santri/2.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item carousel-box">
                              <img src="assets/santri/3.jpeg" class="d-block w-100" alt="...">
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-8 ps-5 position-relative">
               <div class="row">
                   <div class="col-md-6" data-aos="flip-down" duration="2000">
                       <div>
                            <strong>Nama Sekolah :</strong><br><?= $profil['namasekolah'] ?>
                       </div>
                       <br>
                       <div>
                            <strong>Alamat Sekolah :</strong>
                            <br><?= "$profil[desa], $profil[kecamatan], $profil[kabupaten]" ?>
                       </div>
                       <!-- <br>
                        <div>
                            <strong>Fasilitas :</strong>
                            <ul>
                                <li>Ruang Belajar Yang Nyaman</li>
                                <li>Kelas Komputer</li>
                                <li>Halaman Bermain Yang Luas</li>
                                <li>Lokasi strategis</li>
                            </ul>
                        </div> -->
                   </div>
                   <div class="col-md-6">

                        <div class="timeline" >
                           <div class="timelinebox right" >
                                <div class="content py-2 px-3"  data-aos="fade-down-right" data-aos-duration="1000">
                                    <h4  >Extra Kurikuler</h4>
                                    <ul>
                                        <?php foreach($ekskul as $eks): ?>
                                          <li><?= $eks['ekskul'] ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                           </div>
                           <div class="timelinebox right">
                                <div class="content mt-3 py-2 px-3" data-aos="fade-down-right" data-aos-duration="1000" >
                                    <h4 >Fasilitas Sekolah</h4>
                                    <ul>
                                        <?php foreach($fasilitas as $fas): ?>
                                          <li><?= $fas['fasilitas'] ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                           </div>
                       </div>
                   </div>
               </div>
            </div>
        </div>
    </div>
</section>

<!-- data guru  -->
<section id="dataguru bg-primary">
  <div class="container ">
    <h3 class="title text-center p-4">Data Guru</h3>
    <div class="garis bg-success mx-auto rounded-pill mb-5 "></div>
    <div class="row justify-content-center">
      <?php foreach($guru as $g) :?>
        <div class="col-md-3"  data-aos="flip-down" data-aos-duration="3000">
          <div data-id="<?= $g['idGuru'] ?>" class="photobox m-3 m-lg-0 position-relative">
              <div id="box_<?= $g['idGuru'] ?>" class="position-absolute  fade-box  detailbox hg-100 w-100" style="display:none" >
                <div class="text-centertext-light top-50 start-50 translate-middle position-absolute">
                    <a href="/info/guru/<?= $g['namaguru'] ?>" class="fs-4 text-light"><strong><?= $g['namaguru'] ?></strong></a>
                </div>
              </div>
              <img src="assets/photoguru/<?= $g['photo'] ?>" alt="" srcset="">
            </div>
        </div>
      <?php endforeach; ?>

    </div>
    <div class="text-center py-4">

      <a href="/info/guru" class="rounded-pill btn btn-lg bg-success text-light">Semua Data  <i class="bi bi-arrow-right-circle-fill"></i></a>
    </div>
  </div>
</section>

<!-- berita  -->
<section class="bg-light min-hg100" id="berita">
    <div class="container ">
        <h3 class="title text-center p-4">Info Sekolah</h3>
        <div class="garis bg-success mx-auto rounded-pill mb-5 "></div>

        <div class="row justify-content-center">
            <?php foreach ($databerita as $berita) : ?>
            <div class="col-md-3">
                <div class="info-box p-3 mb-3 mb-lg-0"  style="height:100%"  data-aos="zoom-in-down" data-aos-duration="1000">
                    <div class="img-box w-100">
                        <img class="w-100 rounded border-success border-1 border" src="assets/thumbnailberita/<?= $berita['thumbnail'] ?>" alt="">
                    </div>
                    <hr>
                    <h6 class="info-title">
                      <?= $berita['judulBerita'] ?>
                    </h6>
                    <a href="/info/baca/<?= $berita['slug'] ?>">
                      <button class="btn text-dark rounded-pill form-control">
                        Selengkapnya <i class="bi bi-arrow-right-circle-fill"></i> 
                      </button>
                    </a>
                </div>
            </div>
            <?php endforeach ; ?>
        </div>
        <div class="text-center py-4">
          <a href="/info/berita" class="rounded-pill btn btn-lg bg-success text-light">Semua Berita  <i class="bi bi-arrow-right-circle-fill"></i></a>
        </div>
    </div>
</section>
<!-- Glery Kegiatan  -->
<section class="bg-success min-hg100" id="berita">
    <div class="container ">
        <h3 class="title text-center p-4">Galery Kegiatan</h3>
        <div class="garis bg-purple mx-auto rounded-pill mb-5 "></div>
        <div class="container">
          <div class="justify-content-center slick-box">
              <?php foreach ($galeries as $galery) : ?>
                <div class="slick-img-box m-1 border border-3 border-light rounded">
                  <div class="img position-relative">
                    <img src="/assets/galery/<?= $galery['namaGambar'] ?>" onload="cek(<?= $galery['idGambar'] ?>)" id="<?= $galery['idGambar'] ?>" alt="" data-id="<?= $galery['idGambar'] ?>" class="position-absolute top-50 start-50 translate-middle">
                    
                  </div>
                </div>
              <?php endforeach ; ?>
          </div>
        </div>
        
        <div class="text-center py-4">
          <a href="/info/galery" class="rounded-pill btn border border-light shadow btn-lg text-light">Tampilkan Semua  <i class="bi bi-arrow-right-circle-fill"></i></a>
        </div>
    </div>
</section>

<script>
  $(".photobox").mouseover(function(){
    var id = $(this).data('id');
    // var detail = document.getElementById("box_"+id);
    $( "#box_"+id ).show( "fold", 200 );
    // detail.classList.add("d-none");
  })
  $(".photobox").mouseleave(function(){
    var id = $(this).data('id');
    // var detail = document.getElementById("box_"+id);
    $( "#box_"+id ).hide( "fold", 200 );
    // detail.classList.add("d-none");
  })

  function cek(id){
    
      var image = document.getElementById(id);
      var height = image.offsetHeight;
      var width = image.offsetWidth;

      if (height == width){
        image.style.width = "100%";
      }else if (height < width ){
        image.style.height = "100%";
      }else{
        image.style.width = "100%";
      }
  }
</script>
<?= $this->endSection() ?>