<?= $this->extend('templates/layout/layout'); ?>

<?= $this->section('content') ?>
  
<script type="text/javascript">
     $(".sidebar-item").removeClass("active");
     $("#galery").addClass("active");
</script>
<hr>
<div class="container">
    <section class="section">
        <div class="row" id="table-striped">
            <div class="col-12 p-0">
                <div class="card shadow ">
                    <div class="card-header bg-primary text-light mb-3">
                        <strong class="card-title"> <i class="bi bi-person-fill"></i> Tabel Data Guru</strong>
                        <a href="/guru/formguru" class="btn  btn-sm btn-light position-absolute end-0 me-4 top-0 mt-4 rounded-pill d-none d-md-block"><i class="bi bi-plus-circle-fill"></i> Tambah Data</a>
                    </div>
                    <div class="p-3">
                        <div class="row">
                            <div class="col-md-3 col-12 p-2">
                                <form method="post" action="/controlgalery/simpan" enctype="multipart/form-data">
                                    <div class="galery-box">
                                        <input type="file" onchange="preview()" name="gambar" id="images" class="images d-none">
                                        
                                        <label for="" class="imglabel d-none"></label>
                                        <label for="images">
                                        <div  class="img img-prev p-2 p-md-5 border-success rounded">
                                            <img src="/assets/galery/add-photo.svg" id="img-preview" class="rounded im w-100" alt="galery">
                                        </div>
                                        </label>
                                        <div class="input-caption d-none">
                                            <input type="text" name="caption" class="capt form-control rounded-pill text-center" placeholder="Keterangan Gambar">
                                            <button type="submit" class="btn btn-outline-success mt-2 form-control rounded-pill ">Simpan</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <?php foreach($galeries as $galery): ?>
                                <div class="col-md-3 col-6 p-2">
                                    <div class="galery-box">
                                        <div class="img border border-secondary rounded position-relative">
                                            <img src="/assets/galery/<?= $galery['namaGambar'] ?>" class="rounded position-absolute top-50 start-50 translate-middle" id="im-<?= $galery['idGambar'] ?>" alt="galery">
                                            <small class="bg-danger text-light position-absolute top-0 end-0 mt-2 me-2 rounded p-2"><i class="bi bi-trash"></i></small>
                                        </div>
                                        <div class="caption text-center">
                                            <span class="title"><?= $galery['caption'] ?></span>
                                        </div>
                                    </div>
                                </div>
                               <script>
                                   $("#im-<?= $galery['idGambar'] ?>").ready(function(){
                                        var height = $("#im-<?= $galery['idGambar'] ?>").height();
                                        var width = $("#im-<?= $galery['idGambar'] ?>").width();
                                       
                                        if (height == width){
                                            $("#im-<?= $galery['idGambar'] ?>").css('width',"100%");
                                        }else if (height < width ){
                                            $("#im-<?= $galery['idGambar'] ?>").css('height',"100%");
                                        }else{
                                            $("#im-<?= $galery['idGambar'] ?>").css('width',"100%");
                                        }
                                   })
                               </script>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>


$( document ).ready(function() {

    $(".hapusguru").click(function(){
        const id = $(this).data('id');
        $('#hapus').attr('href','controlguru/hapus/'+id);
    })
    $('.images').change(function(){
        const logo = document.querySelector('#images');
        const label = document.querySelector('.imglabel');
        const prev = document.querySelector('#img-preview');

        label.textContent = logo.files[0].name;

        const filePhoto = new FileReader(); 
        filePhoto.readAsDataURL(logo.files[0]);

        filePhoto.onload = function(e){
            prev.src = e.target.result;
        }

        $('.img-prev').removeClass('p-md-5')
        $('.input-caption').removeClass('d-none');
        $('.capt').focus();

    });
  
});
</script>

<?= $this->endSection(); ?>