<?= $this->extend('templates/layout/layout'); ?>

<?= $this->section('content') ?>
  

<script type="text/javascript">
     $(".sidebar-item").removeClass("active");
     $("#profsek").addClass("active");
     $("#ul-profsek").addClass("active");
     $("#ekskul-profsek").addClass("active");
     $("#ekskul-profsek a").addClass("active text-success");
</script>
<hr>
<!-- Modal edit-->
<div class="modal fade" id="modal-edit" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-body">
            <!-- <form  method="post" class="form"> -->
            <div class="form-floating border-bottom">
                <input type="text" class="form-control  border-bottom  border-0" name="eks" id="eks-edit" placeholder="Nama Fasilitas">
                <input type="hidden" class="form-control  border-bottom  border-0" name="id" id="id-edit" placeholder="Nama Fasilitas">
                <label for="fas">Nama Ekskul</label>
            </div>
            <div class="form-floating mt-4 border-bottom    ">
                <textarea class="form-control border-0" placeholder="Keterangan" id="ket-edit" name="ket"></textarea>
                <label for="ket">Keterangan</label>
            </div>

            <div class="tombol mt-3">
                <button class="btn btn-primary rounded-pill tombol-update"><i class="bi bi-arrow-repeat"></i> Update</button>
                <button data-bs-dismiss="modal" class="btn btn-danger rounded-pill"><i class="bi bi-x-circle-fill"></i> Batal</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
  </div>
</div>


<div class="row rounded bg-white mb-5 pb-5">
    <div class="col-md-5">
        <div class="container-fluid my-3">
            <strong> <i class="bi bi-plus-circle-fill"></i> Ekskul Baru</strong>
            <hr>
            <!-- <form  method="post" class="form"> -->
                <div class="form-floating border-bottom">
                    <input type="text" class="form-control  border-bottom  border-0" name="eks" id="eks" placeholder="Nama Fasilitas">
                    <label for="fas">Nama Ekstra Kurikuler</label>
                </div>
                <div class="form-floating mt-4 border-bottom    ">
                    <textarea class="form-control border-0" placeholder="Keterangan" id="ket" name="ket"></textarea>
                    <label for="ket">Keterangan</label>
                </div>
            <!-- </form> -->
            <button class="btn btn-primary mt-2 rounded-pill" id="tombol-simpan" type="submit"><i class="bi bi-save2-fill"></i> Save </button>
        </div>
    </div>
    <div class="col-md-7">
        <div class="container-fluid my-3">
            <strong><i class="bi bi-back"></i> List Ekstrakurikuler</strong>
            <hr>
            <!-- list fasilitas -->
            <div class="list">
                
            </div>
            
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $(".list").load("/load/ekskul"); 

    $("#tombol-simpan").click(function(){
        eks = $("#eks").val();
        ket = $("#ket").val();
        $.ajax({
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            type: 'POST',
            url: "<?= site_url('progres/simpanEkskul')?>",
            data: {ekskul:eks,keterangan:ket},
            success: function(data) {
                // console.log(data)
                $(".list").load("/load/ekskul"); 
                $("#eks").val('');
                $("#eks").focus();
                $("#ket").val('');
            }
        });

    });

    $(".tombol-update").click(function(){
        // var data = $('.form').serialize();
        eks = $("#eks-edit").val();
        ket = $("#ket-edit").val();
        id = $("#id-edit").val();

        $.ajax({
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            type: 'POST',
            url: "<?= site_url('progres/simpanEkskul/')?>"+id,
            data: {ekskul:eks,keterangan:ket},
            success: function(data) {
                $("#modal-edit").modal('hide');
                $(".list").load("/load/ekskul");  
            }
        });
    });


});
</script>

<?= $this->endSection() ?>