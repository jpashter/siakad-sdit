<?= $this->extend('templates/layout/layout'); ?>

<?= $this->section('content') ?>
  

<script type="text/javascript">
     $(".sidebar-item").removeClass("active");
     $("#profsek").addClass("active");
     $("#ul-profsek").addClass("active");
     $("#fasilitas-profsek").addClass("active");
     $("#fasilitas-profsek a").addClass("active text-success");
</script>
<hr>
<!-- Modal edit-->
<div class="modal fade" id="modal-edit" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-body">
            <!-- <form  method="post" class="form"> -->
            <div class="form-floating border-bottom">
                <input type="text" class="form-control  border-bottom  border-0" name="fas" id="fas-edit" placeholder="Nama Fasilitas">
                <input type="hidden" class="form-control  border-bottom  border-0" name="id" id="id-edit" placeholder="Nama Fasilitas">
                <label for="fas">Nama Fasilitas</label>
            </div>
            <div class="form-floating mt-4 border-bottom    ">
                <textarea class="form-control border-0" placeholder="Keterangan" id="ket-edit" name="ket"></textarea>
                <label for="ket">Keterangan</label>
            </div>

            <div class="tombol mt-3">
                <button class="btn btn-primary rounded-pill tombol-update"><i class="bi bi-arrow-repeat"></i> Update</button>
                <button data-bs-dismiss="modal" class="btn btn-danger rounded-pill "><i class="bi bi-x-circle-fill"></i> Batal</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
  </div>
</div>


<div class="row rounded bg-white mb-5 pb-5">
    <div class="col-md-5">
        <div class="container-fluid my-3">
            <strong> <i class="bi bi-card-checklist"></i> Form Fasilitas</strong>
            <hr>
            <!-- <form  method="post" class="form"> -->
                <div class="form-floating border-bottom">
                    <input type="text" class="form-control  border-bottom  border-0" name="fas" id="fas" placeholder="Nama Fasilitas">
                    <label for="fas">Nama Fasilitas</label>
                </div>
                <div class="form-floating mt-4 border-bottom    ">
                    <textarea class="form-control border-0" placeholder="Keterangan" id="ket" name="ket"></textarea>
                    <label for="ket">Keterangan</label>
                </div>
            <!-- </form> -->
            <button class="btn btn-primary mt-2 rounded-pill" id="tombol-simpan" type="submit"><i class="bi bi-save2-fill"></i> Save </button>
        </div>
    </div>
    <div class="col-md-7">
        <div class="container-fluid my-3">
            <strong> <i class="bi bi-tools"></i> Fasilitas</strong>
            <hr>
            <!-- list fasilitas -->
            <div class="list">
                
            </div>
            
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $(".list").load("/load/index"); 
    // $( ".list" ).append( "<p>Test</p>" )
    $("#tombol-simpan").click(function(){
        // var data = $('.form').serialize();
        fas = $("#fas").val();
        ket = $("#ket").val();

        $.ajax({
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            type: 'POST',
            url: "<?= site_url('progres/simpanFasilitas')?>",
            data: {fasilitas:fas,keterangan:ket},
            success: function(data) {
                console.log(data)
                $(".list").load("/load/index");  
                $("#fas").val('');
                $("#fas").focus();
                $("#ket").val('');
            }
        });
    });

    $(".tombol-update").click(function(){
        // var data = $('.form').serialize();
        fas = $("#fas-edit").val();
        ket = $("#ket-edit").val();
        id = $("#id-edit").val();

        $.ajax({
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            type: 'POST',
            url: "<?= site_url('progres/simpanFasilitas/')?>"+id,
            data: {fasilitas:fas,keterangan:ket},
            success: function(data) {
                $("#modal-edit").modal('hide');
                $(".list").load("/load/index");  
            }
        });
    });


});
</script>

<?= $this->endSection() ?>