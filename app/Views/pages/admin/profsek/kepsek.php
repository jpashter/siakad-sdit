<?= $this->extend('templates/layout/layout'); ?>

<?= $this->section('content') ?>
  

<script type="text/javascript">
     $(".sidebar-item").removeClass("active");
     $("#profsek").addClass("active");
     $("#ul-profsek").addClass("active");
     $("#kepsek-profsek").addClass("active");
     $("#kepsek-profsek a").addClass("active text-success");
</script>
<hr>
<div class="p-1 p-md-5 rounded bg-white">
    <div class="row">
        <div class="col-md-12 mb-5">
            <div class="p-3 p-md-5 bg-white">
                <div class="garis bg-success mx-auto rounded-pill mb-5 "></div>
                <!-- <hr> -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="container-fluid mb-3">
                            <img class="w-100 border border-1 border-success shadow" src="/assets/photoguru/<?= $guru['photo'] ?>" alt="" srcset="">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div  style="font-family:ubuntu">
                            <h4><?= $guru['namaguru'].','.$guru['gelar'] ?></h4>
                            <!-- <hr> -->
                            <div class="text-secondary">

                                <small><?= $guru['jabatan'] ?></small> |
                                <small><i><?= "$guru[status] : $guru[niy]" ?></i></small>
                            </div>
                            <hr>
                        </div>
                        
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Jenis Kelamin</strong>
                                <span class="position-absolute end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $guru['kelamin'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Alamat</strong>
                                <span class="position-absolute end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $guru['alamat'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Tanggal Lahir</strong>
                                <span class="position-absolute  end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $guru['tgllahir'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Pendidikan Terahir</strong>
                                <span class="position-absolute top-0 end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $guru['pendidikan'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Perguruan Tinggi</strong>
                                <span class="position-absolute top-0 end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $guru['pt'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Jurusan</strong>
                                <span class="position-absolute end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $guru['jurusan'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Email</strong>
                                <span class="position-absolute end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $guru['email'] ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5 position-relative">
                                <strong>Nomor HP/WA</strong>
                                <span class="position-absolute end-0 me-1">:</span>
                            </div>
                            <div class="col-7">
                                <?= $guru['hp'] ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- form sambutan  -->
    <div class="p-3">
        <form action="/progres/simpantentang" method="post">
            <?php if(!$sambutan == null ): ?>
                <input type="hidden" name="idTentang" value="<?= $sambutan['idTentang'] ?>">
            <?php endif; ?>
            <label for=""> <i class="bi bi-list"></i> Sambutan</label>
            <textarea name="isiTentang" id="sambutanKepsek" class="ckeditor form-control" rows="10"><?= $sambutan==null ? '' : $sambutan['isiTentang'] ;  ?></textarea>
            <input type="hidden" name="tentang" value="Sambutan Kepsek">
            <input type="hidden" name="keterangan" value="Sambutan Kepsek">
            <button type="submit" class="btn btn-primary rounded-pill mt-3 px-3"><i class="bi bi-save"></i> Simpan</button>
        </form>
    </div>
   
</div>
<script>
$(document).ready(function(){
    $(".list").load("/load/ekskul"); 

    $("#tombol-simpan").click(function(){
        eks = $("#eks").val();
        ket = $("#ket").val();
        $.ajax({
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            type: 'POST',
            url: "<?= site_url('progres/simpanEkskul')?>",
            data: {ekskul:eks,keterangan:ket},
            success: function(data) {
                // console.log(data)
                $(".list").load("/load/ekskul"); 
                $("#eks").val('');
                $("#eks").focus();
                $("#ket").val('');
            }
        });

    });

    $(".tombol-update").click(function(){
        // var data = $('.form').serialize();
        eks = $("#eks-edit").val();
        ket = $("#ket-edit").val();
        id = $("#id-edit").val();

        $.ajax({
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            type: 'POST',
            url: "<?= site_url('progres/simpanEkskul/')?>"+id,
            data: {ekskul:eks,keterangan:ket},
            success: function(data) {
                $("#modal-edit").modal('hide');
                $(".list").load("/load/ekskul");  
            }
        });
    });


});
</script>

<?= $this->endSection() ?>