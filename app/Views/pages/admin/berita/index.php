<?= $this->extend('templates/layout/layout'); ?>

<?= $this->section('content') ?>
  
<script type="text/javascript">
     $(".sidebar-item").removeClass("active");
     $("#berita").addClass("active");
</script>
<hr>
<div class="container">
    <section class="section">
        <div class="row" id="table-striped">
            <div class="col-12">
                <div class="card shadow ">
                    <div class="card-header bg-primary text-light mb-3">
                        <strong class="card-title"> <i class="bi bi-newspaper"></i> Tabel Data Berita</strong>
                        <a href="/berita/formberita" class="btn  btn-sm btn-light position-absolute end-0 me-4 top-0 mt-4 rounded-pill d-none d-md-block"><i class="bi bi-plus-circle-fill"></i> Tambah Data</a>
                    </div>
                    <div class="row container mt-3">
                        <div class="col-8 col-7 px-2 px-md-2 col-md-5 mb-3">
                            <div class="input-group mb-2 position-relative">
                                <form action="" method="get" class="w-100">
                                    <input name="q" type="text" class="form-control rounded-pill" placeholder="Cari">
                                </form>
                                <span class="position-absolute translate-middle me-2 top-50 end-0"><i class="bi bi-search"></i></span>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="card-content container pb-3">

                        <!-- mobile view  -->
                        
                        <!-- end mobile view  -->
                            
                             <!-- .alert-primary, .alert-secondary, .alert-success, .alert-danger, .alert-warning, .alert-info, .alert-light, .alert-dark -->
                            
                        <!-- Button trigger modal -->
                      
                        <div class="table-responsive d-none d-md-block">
                            <table class="table table-hover table-striped mb-0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Thumbnail</th>
                                        <th>Judul</th>
                                        <th>Penulis</th>
                                        <th>Kategori</th>
                                        <th>Waktu Publish</th>
                                        <th>PILIHAN</th>
                                    </tr>
                                </thead>
                                
                                <tbody style="font-size:10pt"> 
                                    <?php if ($databerita == null) : ?>
                                        <div class="text-center">
                                            <!-- <img  src="/assets/img/empty.gif" class="mx-auto" style="width:300px" alt=""> -->
                                            <br><span>Tidak Ada Data Untuk Ditampilkan</span>

                                            <br> <a href="/berita">Tampilkan Semua</a>
                                        </div>
                                    <?php else : ?>
                                    <?php foreach($databerita as $berita): ?>
                                      
                                    <tr>  
                                        <td>
                                            <?= $no++ ?>
                                        </td>
                                        <td>
                                            <div class="mx-auto" style="width:100px;overflow:hidden">
                                                <img style="width:100%" src="/assets/thumbnailberita/<?= $berita['thumbnail'] ?>" alt="Thumbnail Berita">
                                            </div>
                                        </td>
                                        <td><?= $berita['judulBerita'] ?></td>
                                        <td><?= $berita['penulis'] ?></td>
                                        <td><?= $berita['keterangan'] ?></td>
                                        <td><?= $berita['created_at'] ?></td>
                                        
                                        <td class="">
                                            <a href="/berita/formberita/<?= $berita['slug'] ?>">
                                                <i class="bi bi-pencil-fill"></i>
                                            </a>
                                            <a href="#confirmhapus" class="hapusberita" data-bs-toggle="modal" data-id="<?= $berita['idBerita'] ?>">
                                                <i class="bi bi-trash-fill"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>

                                    <?php endif ; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Striped rows end -->
</div>

<div class="postion-absolute shadow d-md-none position-fixed me-4 mb-4 bg-success rounded-circle end-0 bottom-0"  style="width: 50px;height: 50px;">
    <div class="positon-relative">
        <a href="/guru/formguru" class="text-light fs-1 position-absolute top-50 start-50 translate-middle">
            <i class="bi bi-plus-circle-dotted"></i>
        </a>
    </div>
</div>

<script>
$(".hapusberita").click(function(){
    const id = $(this).data('id');
    $('#hapus').attr('href','controlberita/hapus/'+id);
})

</script>
<?= $this->endSection(); ?>