<?= $this->extend('templates/layout/layout'); ?>

<?= $this->section('content') ?>
  
<script type="text/javascript">
     $(".sidebar-item").removeClass("active");
     $("#berita").addClass("active");
</script>

<hr>
<form method="post" action="/controlberita/simpan/<?= $edit>0 ? $berita['idBerita'] : '' ;?>" enctype="multipart/form-data">
    <?= csrf_field() ?>
    <div class="container">
        <div class="row">
            <div class="col-12 col-xl-9">
                <div class="card shadow-sm">
                    <div class="card-header">

                        <span class="fs-5 ps-4"><strong>Detail Berita</strong></span>
                        <!-- hidden input  -->
                        <input type="hidden" name="penulis" value="Tim Kreatif">
                        <input type="hidden" name="keterangan" value="berita">
                    </div>
                    <div class="card-body p-0 p-md-3">
                        <div class="container">
                            <div class="form-group">
                                <label for="">Judul Berita</label>
                                <input value="<?= $edit>0 ? $berita['judulBerita'] : old('judulBerita') ;?>" type="text" class="form-control rounded-pill" name="judulBerita" autofocus>
                            </div>
                            <div class="form-group">
                                <label for="">Isi Berita</label>
                                <textarea name="isiBerita" id="isiberita" class="ckeditor form-control" rows="10"><?= $edit>0 ? $berita['isiBerita'] : old('isiBerita') ;?></textarea>
                            </div>
                            

                            <hr>
                            <div class="row text-center  my-3">
                                <button type="submit" class="btn btn-success rounded-pill py-2">  Simpan</button>

                                <a class="mt-3" href="/panel/guru">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-xl-3">
                <div class="card shadow-sm">
                    <div class="card-header">
                        <h4><i class="bi bi-image"></i> Thumbnail</h4>
                    </div>
                    <div class="card-body"> 
                        <label for="logo" class="form-label logoLabel  d-none"></label>
                        <div class="fileUpload  p-0 mx-auto">
                            <input class="upload" style="height:100%" type="file" onchange="preview()" id="logo" name="photo">
                            <img src="/assets/thumbnailberita/<?= $edit>0 ? $berita['thumbnail'] : 'thumbnail.jpg' ;?>" class="img-thumbnail img-preview img-fluid" style="width:100%" alt="Logo">
                            <span></span>
                        </div>
                        <div>
                            <label for="">Kategori</label>
                            <textarea name="tag"  rows="3" class="form-control"><?= $edit>0 ? $berita['tag'] : old('tag') ;?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


<?= $this->endSection(); ?>