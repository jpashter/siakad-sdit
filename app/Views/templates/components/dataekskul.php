<?php if($ekskul == null): ?>
    <h5 class="text-center">Tidak Ada Ekstra Kurikuler Untuk Ditampilkan</h5>
<?php endif; ?>

<?php foreach($ekskul as $fas): ?>
    <div class="my-3 rounded p-2 p-md-4 shadow bg-secondary text-light position-relative">
        <div><strong id="fas-name"><?= $fas['ekskul'] ?></strong></div>
        <hr>
        <div id="fas-caption">
            <?= $fas['keterangan'] ?>
        </div>
        <!-- tombol hapus  -->
        <a class="bg-light text-secondary px-2 rounded-pill position-absolute top-0 end-0 mt-3 me-5 btn btn-sm"><i class="bi bi-trash2 btn-hapus" onclick="hapus(<?= $fas['idEkskul'] ?>)"></i></a>
        <!-- tombol edit  -->
        <a  class="bg-warning text-secondary px-2 rounded-pill position-absolute top-0 end-0 mt-3 me-2 btn btn-sm" onclick="edit(<?= $fas['idEkskul'] ?>)"><i class="bi bi-pencil"></i></a>
    </div>
<?php endforeach; ?>

<script>
    function hapus(id)
    {
        $.ajax({
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            type: 'POST',
            url: "<?= site_url('progres/hapusEkskul/')?>"+id,
            // data: {idfas:id},
            success: function(data) {
                $(".list").load("/load/ekskul"); 
            }
        });
    } 

    function edit(id)
    {
        $("#modal-edit").modal('show');
        $.ajax({
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            type: 'POST',
            url: "<?= site_url('progres/findEks/')?>"+id,
            // data: {idfas:id},
            success: function(data) {
                var res = JSON.parse(data) 
                $("#eks-edit").val(res.ekskul.ekskul)
                $("#ket-edit").val(res.ekskul.keterangan)
                $("#id-edit").val(res.ekskul.idEkskul)
            }
        });
    } 
</script>