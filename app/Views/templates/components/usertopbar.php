<nav style="font-family:Roboto" class="navbar usernav navbar-expand-lg navbar-dark  bg-success">
  <div id="topbar" class=" container">
    <a class="navbar-brand bg-light rounded px-0" href="/">
        <img src="/assets/img/saidalamin.png" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse bg-success p-3 p-md-0 rounded menu" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto  px-0 mb-2 mb-lg-0">
        <li class="nav-item link-topbar">
          <a class="nav-link" aria-current="page" href="/info/profil">Profil Sekolah</a>
        </li>
        <li class="nav-item link-topbar">
          <a class="nav-link" href="/info/siswa">Data Siswa</a>
        </li>
        <li class="nav-item link-topbar">
          <a class="nav-link" href="/info/guru" tabindex="-1" aria-disabled="true">Data Guru</a>
        </li>
        <li class="nav-item link-topbar">
          <a class="nav-link" href="/info/berita" tabindex="-1" aria-disabled="true">Berita</a>
        </li>
        <li class="nav-item link-topbar">
          <a class="nav-link" href="/info/galery" tabindex="-1" aria-disabled="true">Galery</a>
        </li>
        <!-- <li class="nav-item link-topbar dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Perangkat
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="#">RPP</a></li>
            <li><a class="dropdown-item" href="#">Silabus</a></li>
            <li><a class="dropdown-item" href="#">Kalender Pendidikan</a></li>
          </ul>
        </li> -->
      </ul>
      <form action="/info/berita" method="get" class="d-flex position-relative">
        <input name="q" class="input-search bg-outline-light pe-5 rounded-pill px-4 form-control me-2" type="search" placeholder="Cari Informasi" aria-label="Search">
        <span class="text-secondary end-0 top-0 mt-2 me-4 position-absolute"><i class="bi bi-search"></i></span>
        <!-- <button class="btn-search btn btn-outline-light" type="submit"><i class="bi bi-search"></i></button> -->
      </form>
    </div>
  </div>
</nav>