<div id="sidebar" class="active">
    <div class="sidebar-wrapper active">
        <div class="sidebar-header">
            <div class="d-flex justify-content-between">
                <div class="logo" style="width:100%">
                    <a href="/panel">
                        <img src="/assets/img/saidalamin.png" alt="Logo" style="width:100%;height:90%" >
                    </a>
                </div>
                <div class="toggler">
                    <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                </div>
            </div>
        </div>
        <div class="sidebar-menu">
            <ul class="menu">
                <li class="sidebar-item has-sub" id="profsek"  >
                    <a href="/panel/profsek" class='sidebar-link'>
                        <i class="bi bi-hexagon-fill"></i>
                        <span>Profil Sekolah</span>
                    </a>
                    <ul class="submenu" style="margin-left:-30px" id="ul-profsek">
                        <li class="submenu-item" id="detail-profsek">
                            <a href="/panel/profsek"> <i class="bi bi-ui-radios"></i> Detail Sekolah</a>
                        </li>
                        <li class="submenu-item" id="fasilitas-profsek">
                            <a href="/panel/fasilitas"> <i class="bi bi-toggles2"></i> Fasilitas Sekolah</a>
                        </li>
                        <li class="submenu-item" id="ekskul-profsek">
                            <a href="/panel/ekskul"> <i class="bi bi-back"></i> Ekstra Kurikuler</a>
                        </li>
                        <li class="submenu-item" id="kepsek-profsek">
                            <a href="/panel/kepsek"> <i class="bi bi-person-fill"></i> Kepala Sekolah</a>
                        </li>
                    </ul>
                </li>

                <!-- <li class="sidebar-item  has-sub">
                    <a href="#" class='sidebar-link'>
                        <i class="bi bi-stack"></i>
                        <span>Components</span>
                    </a>
                    <ul class="submenu ">
                        <li class="submenu-item ">
                            <a href="component-alert.html">Alert</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="component-badge.html">Badge</a>
                        </li>
                        <li class="submenhttp://192.168.8.102:8080/u-item ">
                            <a href="component-breadcrumb.html">Breadcrumb</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="component-button.html">Button</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="component-card.html">Card</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="component-carousel.html">Carousel</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="component-dropdown.html">Dropdown</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="component-list-group.html">List Group</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="component-modal.html">Modal</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="component-navs.html">Navs</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="component-pagination.html">Pagination</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="component-progress.html">Progress</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="component-spinner.html">Spinner</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="component-tooltip.html">Tooltip</a>
                        </li>
                    </ul>
                </li> -->

                <!-- <li class="sidebar-item  has-sub">
                    <a href="#" class='sidebar-link'>
                        <i class="bi bi-collection-fill"></i>
                        <span>Extra Components</span>
                    </a>
                    <ul class="submenu ">
                        <li class="submenu-item ">
                            <a href="extra-component-avatar.html">Avatar</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="extra-component-sweetalert.html">Sweet Alert</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="extra-component-toastify.html">Toastify</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="extra-component-rating.html">Rating</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="extra-component-divider.html">Divider</a>
                        </li>
                    </ul>
                </li> -->

                <!-- <li class="sidebar-item  has-sub">
                    <a href="#" class='sidebar-link'>
                        <i class="bi bi-grid-1x2-fill"></i>
                        <span>Layouts</span>
                    </a>
                    <ul class="submenu ">
                        <li class="submenu-item ">
                            <a href="layout-default.html">Default Layout</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="layout-vertical-1-column.html">1 Column</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="layout-vertical-navbar.html">Vertical with Navbar</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="layout-horizontal.html">Horizontal Menu</a>
                        </li>
                    </ul>
                </li> -->

                <li class="sidebar-item  " id="guru">
                    <a href="/guru" class='sidebar-link'>
                        <i class="bi bi-people-fill"></i>
                        <span>Data Guru</span>
                    </a>
                </li>
                <li class="sidebar-item  " id="siswa">
                    <a href="/siswa" class='sidebar-link'>
                        <i class="bi bi-people"></i>
                        <span>Data Siswa</span>
                    </a>
                </li>
                <li class="sidebar-item  has-sub" id="surat">
                    <a href="#" class='sidebar-link'>
                        <i class="bi bi-pen-fill"></i>
                        <span>Surat Menyurat</span>
                    </a>
                    <ul class="submenu ">
                        <li class="submenu-item ">
                            <a href="/surat/keterangansiswa">Keterangan Siswa</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item" id="galery">
                    <a href="/galery" class='sidebar-link'>
                        <i class="bi bi-images"></i>
                        <span>Galery Kegiatan</span>
                    </a>
                </li>

                <li class="sidebar-title">Fitur Extra</li>

                <li class="sidebar-item" id="berita">
                    <a href="/berita" class='sidebar-link'>
                        <i class="bi bi-newspaper"></i>
                        <span>Berita</span>
                    </a>
                </li>
                <li class="sidebar-item" id="slide">
                    <a href="/tabungan" class='sidebar-link'>
                        <i class="bi bi-credit-card-2-front-fill"></i>
                        <span>Tabungan Siswa</span>
                    </a>
                </li>
                
            </ul>
        </div>
        <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
    </div>
</div>