
            <footer class="text-light">
                <div class="bg-darkgreen p-5 fs-1 text-center">
                    <h5 class="title text-center">Find Us :</h5>
                    <a href="" class="text-light"><i class="bi bi-facebook"></i></a>
                    <a href="" class="text-light"><i class="bi bi-youtube"></i></a>
                    <a href="" class="text-light"><i class="bi bi-instagram"></i></a>
                </div>
                <div class="bg-moredarkgreen p-3 text-center">
                    <small>poweredby@jupriadi1922</small>
                </div>
            </footer>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="saved" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-body py-4">
                    <div class="text-center text-success" style="font-size:50pt;"><i class="bi bi-emoji-heart-eyes"></i></div>
                    <p class="text-center">
                        <?= session()->getFlashData('saved') ?>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <?php if(session()->get('saved')) : ?>
        <script type="text/javascript">
            $(window).on('load', function() {
                $('#saved').modal('show');
            });
        </script>
    <?php endif ?>

    <script>
        setTimeout(function() {
            $("#saved").modal('hide')
        }, 2000);

    </script>

    <script src="/assets/js/perfect-scrollbar.js"></script>
    <script src="/assets/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/apexcharts.js"></script>
    <script src="/assets/js/dashboard.js"></script>
    <script src="/assets/js/main.js"></script>
    <script src="/assets/js/aos.js"></script>
    <script src="/assets/glightbox/js/glightbox.min.js"></script>
    <script src="/assets/cropper/cropper.js"></script>
    <script src="/assets/js/bootstrap-datepicker.min.js"></script>
    <script src="/assets/js/lightbox/src/js/lightbox.js"></script>
    <!-- <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script> -->
    <script src="/assets/js/slick/slick/slick.min.js"></script>

    <script>   
        AOS.init(); 

        $('.slick-box').slick({
            lazyLoad: 'ondemand',
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3
        });
    </script>
    <script>
        $(document).ready(function(){
            
            $(".loading").addClass("d-none");
        })
    </script>
    <script>
        $( window ).scroll( function () {
    if ( $(document).scrollTop() > 50 ) {
      $(".usernav").addClass("fixed-top bg-light navbar-light ");
      $(".usernav").removeClass("navbar-dark");
      $(".janggel ").removeClass("d-md-none");
      $(".menu").addClass("bg-light");
      $(".menu").removeClass("bg-success");


    } else {
    $(".janggel ").addClass("d-md-none");
      $(".usernav").removeClass("fixed-top bg-light navbar-light");
      $(".usernav").addClass("navbar-dark");
      $("#tgr-nav").removeClass("btn-outline-dark");
      $("#tgr-nav").addClass("btn-outline-light");

      $(".menu").removeClass("bg-light");
      $(".menu").addClass("bg-success p-3 p-md-0");
      $(".btn-search").addClass('bg-light,')
    }
  });

        function preview(){
                const logo = document.querySelector('#logo');
                const label = document.querySelector('.logoLabel');
                const prev = document.querySelector('.img-preview');

                label.textContent = logo.files[0].name;

                const filePhoto = new FileReader(); 
                filePhoto.readAsDataURL(logo.files[0]);

                filePhoto.onload = function(e){
                    prev.src = e.target.result;
                }
      };

      $('#date').datepicker({
		format: 'yyyy-mm-dd',
		daysOfWeekDisabled: "0",
		autoclose:true
    });

    

    </script>
</body>

</html>