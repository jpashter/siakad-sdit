<?php if($fasilitas == null): ?>
    <h5 class="text-center">Tidak Ada Fasilitas Untuk Ditampilkan</h5>
<?php endif; ?>

<?php foreach($fasilitas as $fas): ?>
    <div class="my-3 rounded p-2 p-md-4 shadow bg-secondary text-light position-relative">
        <div><strong id="fas-name"><?= $fas['fasilitas'] ?></strong></div>
        <hr>
        <div id="fas-caption">
            <?= $fas['keterangan'] ?>
        </div>
        <!-- tombol hapus  -->
        <a class="bg-light text-secondary px-2 rounded-pill position-absolute top-0 end-0 mt-3 me-5 btn btn-sm"><i class="bi bi-trash2 btn-hapus" onclick="hapus(<?= $fas['idFas'] ?>)"></i></a>
        <!-- tombol edit  -->
        <a  class="bg-warning text-secondary px-2 rounded-pill position-absolute top-0 end-0 mt-3 me-2 btn btn-sm" onclick="edit(<?= $fas['idFas'] ?>)"><i class="bi bi-pencil"></i></a>
    </div>
<?php endforeach; ?>

<script>
    function hapus(id)
    {
        $.ajax({
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            type: 'POST',
            url: "<?= site_url('progres/hapusFasilitas/')?>"+id,
            // data: {idfas:id},
            success: function(data) {
                $(".list").load("/load/index"); 
            }
        });
    } 

    function edit(id)
    {
        $("#modal-edit").modal('show');
        $.ajax({
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            type: 'POST',
            url: "<?= site_url('progres/findFas/')?>"+id,
            // data: {idfas:id},
            success: function(data) {
                var res = JSON.parse(data) 
                $("#fas-edit").val(res.fasilitas.fasilitas)
                $("#ket-edit").val(res.fasilitas.keterangan)
                $("#id-edit").val(res.fasilitas.idFas)
            }
        });


    } 
</script>