
<div class="position-sticky top-0 right-0 bg-white p-3">
    <h4 class="title">Recomended News</h4>
    <hr>
    
    <?php $x=1; foreach($popular as $pop): ?>
        <a href="/info/baca/<?= $pop['slug'] ?>" class="text-secondary">
        <div class="row">
            <div class="col-2 fs-1">
                <strong style="font-family:'Ubuntu Mono, Bold'">#<?= $x++ ?></strong>
            </div>
            <div class="col-10 fs-6">
                <strong><?= $pop['judulBerita'] ?></strong>
                <br> <?= $pop['penulis'] ?>, <?= substr($pop['created_at'],0,10) ?>
            </div>
        </div>
        <hr>
        </a>
    <?php endforeach;?>
</div>