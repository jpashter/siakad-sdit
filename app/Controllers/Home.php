<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$profil = $this->profsek->first();
		$guru = $this->guru->paginate(4);
		$galeries = $this->galery->paginate(8);
		$berita = $this->berita->paginate(10);
		$jumguru = $this->guru->countAll();
		$jumsiswa = $this->siswa->countAll();
		$jumberita = $this->berita->countAll();
		// dd($jumsiswa);
		$kepsek = $this->guru->where('jabatan', 'Kepala Sekolah')->first();
		$findSambutan = $this->tentang->where('keterangan', 'Sambutan Kepsek')->first();
		$fasilitas = $this->fas->getAll();
		$data = [
			'judul' => 'SDIT Said Alamin',
			'profil' => $profil,
			'guru' => $guru,
			'databerita' => $berita,
			'galeries' => $galeries,
			'jumsiswa' => $jumsiswa,
			'jumguru' => $jumguru,
			'jumberita' => $jumberita,
			'ekskul' => $this->ekskul->getAll(),
			'kepsek' => $kepsek,
			'sambutan' => $findSambutan,
			'fasilitas' => $fasilitas,
		];
		return view('/pages/user/index',$data);
	}
}
