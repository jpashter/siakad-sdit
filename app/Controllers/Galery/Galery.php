<?php

namespace App\Controllers\Galery;

use App\Controllers\BaseController;

class Galery extends BaseController
{
	public function index()
	{
		$galeries = $this->galery->getAll();
		$data = [
			'judul' => "Galery",
			'judulhalaman' => 'Galery Kegiatan',
			'galeries' => $galeries,
		];
		return view('pages/admin/galery/index',$data);
	}
}
