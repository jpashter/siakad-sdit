<?php

namespace App\Controllers\Galery;

use App\Controllers\BaseController;

class Controlgalery extends BaseController
{
	public function simpan()
	{
		$postes = $this->request->getVar();
		$img = $this->request->getFile('gambar');
		$namagambar = $img->getRandomname();
		$data=[
			'namaGambar' => $namagambar,
			'caption' => $postes['caption'],
		];
		// dd($data);

		$pindah = $img->move('assets/galery/',$namagambar);
		

		if($pindah){

			$simpan = $this->galery->save($data);
		
			if($simpan){
				session()->setFlashdata('saved','Galery Berhasil Ditambah..!');
				return redirect()->to('/galery');
			}else{
				session()->setFlashdata('error','Gagal Menyimpan Data..!');
				return redirect()->to('/galery');
			}
		}else{
			session()->setFlashdata('error','Gagal Menyimpan Gambar..!');
			return redirect()->to('/galery');
		}
	}
}
