<?php

namespace App\Controllers\Berita;

use App\Controllers\BaseController;

class Berita extends BaseController
{
	public function index()
	{
		$key = $this->request->getVar('q');
		$berita = $this->berita->getBerita($key);
		// dd($berita);
		$currentPage = $this->request->getVar('page') ? $this->request->getVar('page'):1;
        $no = (10*($currentPage-1)+1);
		$data = [
			'judul' => "Berita",
			'judulhalaman' => 'Data Berita',
			'databerita' => $berita,
			'pager' => $this->berita->pager,
			'no' => $no,
		];
		// dd($data);
		return view('pages/admin/berita/index',$data);
	}

	public function formberita($slug=false)
	{
		if($slug == false){
			$id = false;
		}else{
			$getbyslug = $this->berita->getByslug($slug);
			$id = $getbyslug['idBerita'];
		};
		$data = [
			'judul' => "Berita Baru",
			'judulhalaman' => 'Form Berita',
            'validation' => \Config\Services::validation(),
		];
		if(!$id==false):
			$data['berita'] = $getbyslug;
			$data['edit'] = 1;
		else:
			$data['edit'] = 0;
			$data['berita'] = "";
		endif;
		// dd($data);
		return view('pages/admin/berita/formberita',$data);
	}
}
