<?php

namespace App\Controllers\Berita;

use App\Controllers\BaseController;

class Controlberita extends BaseController
{
	public function index()
	{
		//
	}
	public function simpan($id=false)
	{
		$data=[];
        $posts = $this->request->getPost();
        $photo = $this->request->getFile('photo');
        // dd($photo);
        foreach($posts as $post => $value):
            $data[$post] = $value;
        endforeach;
        // kelola gambar
        
        $slug       = url_title($data['judulBerita'], '_', TRUE);
		$data['slug'] = $slug;
		// dd($data);
        if(!$id == false):
            $data['idBerita'] = $id;
            $find = $this->berita->find($id);
            if($photo == ""):
                $data['thumbnail'] = $find['thumbnail'];
            else:
                $namaphoto = $photo->getRandomName();
                $data['thumbnail'] = $namaphoto;
                if(!($find['thumbnail']=='thumbnail.jpg')):
                    unlink('assets/thumbnailberita/'.$find['thumbnail']);
                endif;
                $photo->move('assets/thumnailberita/',$namaphoto);
            endif;
			$alert = "Berita Berhasil Update.!";
        else:
            if($photo == ""):
                $data['thumbnail'] = 'thumbnail.jpg';
            else:
                $namaphoto = $photo->getRandomName();
                $data['thumbnail'] = $namaphoto;
                $photo->move('assets/thumbnailberita/',$namaphoto);
            endif;
			$alert = "Berita Baru Berhasil Ditambahkan.!";
        endif;
		
        $simpan = $this->berita->save($data);
        if ($simpan):
            session()->setFlashdata('saved',$alert);
            return redirect()->to('/berita');
        endif;
        
	}
}
