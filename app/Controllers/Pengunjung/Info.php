<?php

namespace App\Controllers\Pengunjung;

use App\Controllers\BaseController;

class Info extends BaseController
{
	public function index()
	{
		// $slug = $this->request->getVar('');
		// dd($slug);
	}
	public function berita()
	{
		$key = $this->request->getVar('q');
		
		$aktifslide= $this->berita->first();
		$slide = $this->berita->where('idBerita !=', $aktifslide['idBerita'])->paginate(2);
		if($key==null){
			$allnews = $this->berita;
		}else{
			$allnews = $this->berita->search($key);
		}

		$data = [
			'judul' => 'Berita',
			'slide' => $slide,
			'terbaru' => $aktifslide,
			'allnews' => $allnews->paginate(8,'berita'),
			'pager' => $this->berita->pager,
			'popular' => $this->popularpost(),
		];

		return view('/pages/user/berita/index',$data);
	}
	public function baca($slug){
		$url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s" : "") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

		$berita = $this->berita->getByslug($slug);
		$date = substr($berita['updated_at'],0,10);
		$time = substr($berita['updated_at'],11);
		// dd($date);
		$datetime = \DateTime::createFromFormat("Y-m-d", $date)->format("d M Y");
		// dd($datetime);
		$publishedDate =$datetime; 
		$data = [
			'judul' => $berita['judulBerita'],
			'berita' => $berita,
			'tglpublish' => $publishedDate,
			'waktupublish' => $time,
			'url' => $url,
			'popular' => $this->popularpost(),
		];
		return view('/pages/user/berita/bacaberita',$data);
	}

	public function guru($detail=false)
	{
		if(!$detail == false )
		{
			return $this->detailguru($detail);
		}else{
			return $this->semuadataguru();
		}
	}

	private function detailguru($detail)
	{
		$guru = $this->guru->getByname($detail);
		$data = [
			'judul' => 'Guru | '.$guru['namaguru'],
			'guru' => $guru,

			'popular' => $this->popularpost(),
		];
		return view('/pages/user/guru/detailguru',$data);
	}

	private function semuadataguru()
	{
		$guru = $this->guru->where('jabatan', 'Guru Mapel')->findAll();
		$staf = $this->guru->where('jabatan !=', 'Guru Mapel')->where('jabatan !=','Kepala Sekolah')->findAll();
		$kepsek = $this->guru->where('jabatan', 'Kepala Sekolah')->first();
		$waka = $this->guru->like('jabatan','waka')->findAll();
		// dd($guru);

		$data = [
			'judul' => 'Data Guru',
			'guru' => $guru,
			'kepsek' => $kepsek,
			'waka' => $waka,
			'staf' => $staf,
		];

		return view('/pages/user/guru/index',$data);
	}

	private function popularpost()
	{
		return $this->berita->orderBy('title', 'RANDOM')->paginate(3);
	}

	public function galery()
	{
		$galery = $this->galery->paginate(6, 'galery');
		$data = [
			'judul' => 'Galery Kegiatan',
			'popular' => $this->popularpost(),
			'galeries' => $galery,
			'pager'=>$this->galery->pager,
		];


		return view('/pages/user/galery/index',$data);
	}

	public function siswa($detail=false)
	{
		if(!$detail == false )
		{
			return $this->detailsiswa($detail);
		}else{
			return $this->semuasiswa();
		}


	}
	private function semuasiswa()
	{
		$kelas1 = $this->siswa->where('kelas', 1)->findAll();
		$kelas2 = $this->siswa->where('kelas', 2)->findAll();
		$kelas3 = $this->siswa->where('kelas', 3)->findAll();
		$kelas4 = $this->siswa->where('kelas', 4)->findAll();
		$kelas5 = $this->siswa->where('kelas', 5)->findAll();
		$kelas6 = $this->siswa->where('kelas', 6)->findAll();
		$semua = $this->siswa->findAll();
		// dd($guru);

		$data = [
			'judul' => 'Siswa',
			'semuasiswa' => $semua,
		];

		return view('/pages/user/siswa/index',$data);
	}

	private function detailsiswa($detail)
	{
		$siswa = $this->siswa->getByname($detail);
		$data = [
			'judul' => 'Siswa | '.$siswa['namasiswa'],
			'siswa' => $siswa,
			'popular' => $this->popularpost(),
		];
		return view('/pages/user/siswa/detailsiswa',$data);
	}

	public function profil()
	{
		$profil = $this->profsek->first();
		$alamat = "Desa.$profil[desa], Kec.$profil[kecamatan], Kab.$profil[kabupaten], Prov.$profil[provinsi]";
		$data = [
			'judul' => 'Profil Sekolah',
			'popular' => $this->popularpost(),
			'prof' => $profil,
			'alamat' => $alamat,
		];
		return view('/pages/user/profil/index',$data);
	}
}
