<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Progres extends BaseController
{
	
    public function ubahprofil()
    {

        $id=$this->request->getVar('id');
        $logo = $this->request->getFile('logo');
        $find = $this->profsek->find($id);
        // dd( );
        if($logo->getSize() == 0):
            $logoname = $find['logo'];
        else :
            $logoname = $logo->getRandomName();

            if($find['logo']=='sekolah.png'):
                $logo->move('assets/img/',$logoname);
            else:
                $logo->move('assets/img/',$logoname);
                unlink('assets/img/'.$find['logo']);
            endif;
            
        endif;
        
        // dd($find);
        $data = [
            'id' => $id,
            'namasekolah' => htmlspecialchars($this->request->getVar('namasekolah')),
            'npsn' => htmlspecialchars($this->request->getVar('npsn')),
            'nis' => htmlspecialchars($this->request->getVar('nis')),
            'nss' => htmlspecialchars($this->request->getVar('nss')),
            'hp' => htmlspecialchars($this->request->getVar('hp')),
            'email' => htmlspecialchars($this->request->getVar('email')),
            'website' => htmlspecialchars($this->request->getVar('website')),
            'provinsi' => htmlspecialchars($this->request->getVar('provinsi')),
            'kabupaten' => htmlspecialchars($this->request->getVar('kabupaten')),
            'kecamatan' => htmlspecialchars($this->request->getVar('kecamatan')),
            'desa' => htmlspecialchars($this->request->getVar('desa')),
            'logo' => $logoname,
        ];
        // dd($data);
        $simpan = $this->profsek->update($id, $data);
        if($simpan)
        {
            session()->setFlashdata('saved','Data Berhasil Disimpan..!');
            return redirect()->to('/panel/profsek/');
        }
    }

    public function simpansiswa($id = false)
    {
        $data=[];
        $posts = $this->request->getPost();
        $photo = $this->request->getFile('photo');
        // dd($photo);
        foreach($posts as $post => $value):
            $data[$post] = htmlspecialchars($value);
        endforeach;
        // dd($data);

        if(!($id == false)):
            $data['idSiswa'] = $id;
            $find = $this->siswa->find($id);
            if($photo == ""):
                $data['photo'] = $find['photo'];
            else:
                $namaphoto = $photo->getRandomName();
                $data['photo'] = $namaphoto;
                if(!($find['photo']=='siswa.png')):
                    unlink('assets/photosiswa/'.$find['photo']);
                endif;
                $photo->move('assets/photosiswa/',$namaphoto);
            endif;

        else:
            if($photo == ""):
                $data['photo'] = 'siswa.png';
            else:
                $namaphoto = $photo->getRandomName();
                $data['photo'] = $namaphoto;
                $photo->move('assets/photosiswa/',$namaphoto);
            endif;

        endif;

        $simpan = $this->siswa->save($data);

        if ($simpan):
            session()->setFlashdata('saved','Siswa Berhasil Ditambah..!');
            return redirect()->to('/panel/siswa/');
        endif;
    }
    public function hapussiswa($id)
    {
        $find = $this->siswa->find($id);
        // dd($find);
        if(!($find['photo'] == 'siswa.png')){
            unlink('assets/photosiswa/'.$find['photo']);
        }

        $this->siswa->delete($id);
        session()->setFlashdata('saved','Data Berhasil Dihapus..!');
        return redirect()->to('/panel/siswa/');
    }
    public function simpanslide($id=false)
    {
        $file = $this->request->getFile('photo');
        $photoname = $file->getRandomName();
        $pindah = $file->move('assets/slide/',$photoname);
        $data = [
            'deskripsi' =>  $this->request->getVar('desk'),
            'subdeskripsi' =>  $this->request->getVar('subdesk'),
            'file' =>  $photoname,
        ];

        $this->slide->save($data);
        session()->setFlashdata('saved','Data Berhasil Disimpan..!');
        return redirect()->to('/panel/slide/');
    }

    public function simpanFasilitas($id=false)
    {
        $fas = $this->request->getVar('fasilitas');
        $ket = $this->request->getVar('keterangan');
        $data = [
            'fasilitas' => $fas,
            'keterangan' => $ket,
        ];
        
        (!$id == false) ? $data['idFas'] = $id : '' ;

       $save = $this->fas->save($data);;
       if($save){
           $status=200;$error=0;
           $saved_data=[
                'fas'=> $fas,
                'ket' => $ket,
           ];
        }else{
            $status=500;$error=1;
        }
        $result=[
            'status'=>$status,
            'error'=>$error,
            'data' => $saved_data,
        ];
       return  json_encode($result,true);
    }
    
    public function simpanEkskul($id=false)
    {
        $eks = $this->request->getVar('ekskul');
        $ket = $this->request->getVar('keterangan');
        $data = [
            'ekskul' => $eks,
            'keterangan' => $ket,
        ];
        
        (!$id == false) ? $data['idEkskul'] = $id : '' ;

       $save = $this->ekskul->save($data);
       if($save){
           $status=200;$error=0;
           $saved_data=[
                'eks'=> $eks,
                'ket' => $ket,
           ];
        }else{
            $status=500;$error=1;
            $saved_data = null;
        }
        $result=[
            'status'=>$status,
            'error'=>$error,
            'data' => $saved_data,
        ];
       return  json_encode($result,true);
    }

    public function hapusFasilitas($id)
    {
        $fas = $this->request->getVar('id');
        $delete = $this->fas->hapus($id);

        $result = ['status' => $delete];

        return  json_encode($result,true);
    }
    public function hapusEkskul($id)
    {
        $eks = $this->request->getVar('id');
        $delete = $this->ekskul->hapus($id);

        $result = ['status' => $delete];

        return  json_encode($result,true);
    }

    public function findFas($id)
    {
        $fas = $this->fas->find($id);
        ($fas == null) ? $status = 404 : $status=200;
        $data = [
            'status' => $status,
            'fasilitas' => $fas, 
        ];
        return json_encode($data, true);
    }
    public function findEks($id)
    {
        $eks = $this->ekskul->find($id);
        ($eks == null) ? $status = 404 : $status=200;
        $data = [
            'status' => $status,
            'ekskul' => $eks, 
        ];
        return json_encode($data, true);
    }

    public function simpantentang()
    {
        $postes = $this->request->getPost();
        $save_data = [];
        foreach($postes as $post => $val):
            $save_data[$post] = $val;
        endforeach;
        $slug       = url_title($save_data['tentang'], '_', TRUE);
        $save_data['slug'] = $slug;
        $save = $this->tentang->save($save_data);
        if($save) {
            session()->setFlashdata('saved','Data Berhasil Disimpan..!');
            return redirect()->to('/panel/kepsek/');
        }
    }
}
