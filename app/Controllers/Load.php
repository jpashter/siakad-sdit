<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Load extends BaseController
{
	public function index()
	{
		$getAll = $this->fas->getAll();
		$load_data = [
            'fasilitas' => $getAll,
		];
		return view('/templates/components/datafasilitas', $load_data);
	}

	public function ekskul()
	{
		$getAll = $this->ekskul->getAll();
		$load_data = [
            'ekskul' => $getAll,
		];
		return view('/templates/components/dataekskul', $load_data);

	}
}
