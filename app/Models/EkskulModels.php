<?php

namespace App\Models;

use CodeIgniter\Model;

class EkskulModels extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'ekskul';
	protected $primaryKey           = 'idEkskul';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDeletes       = false;
	protected $protectFields        = true;
	protected $allowedFields        = ['ekskul','keterangan'];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';

	public function getAll()
	{
		return $this->orderBy('created_at','DESC')->findAll();
	}
	public function hapus($id)
	{
		$hapus = $this->where('idEkskul',$id)->delete();
		if($hapus) {
			$status = 200;
		}else{
			$status = 500;
		}
		return $status;
	}
}
