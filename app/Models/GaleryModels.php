<?php

namespace App\Models;

use CodeIgniter\Model;

class GaleryModels extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'galery';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDeletes       = false;
	protected $protectFields        = true;
	protected $allowedFields        = ['namaGambar','caption','keterangan'];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	public function getAll()
	{
		return $this->orderBy('idGambar','DESC')->findAll();
	}
}
