<?php

namespace App\Models;

use CodeIgniter\Model;

class BeritaModels extends Model
{
	protected $table                = 'berita';
	protected $primaryKey           = 'idBerita';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
            'judulBerita',
            'slug',
            'isiBerita',
            'keterangan',
            'tag',
            'thumbnail',
            'penulis',
        ];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];

	public function getBerita($key)
	{
		if($key == null ){
			return $this->paginate(10);
		}else{
			return $this->like('judulBerita', $key)->orLike('penulis', $key)->paginate(10);
		}
	}
	public function getByslug($slug)
	{
		$find = $this->where('slug',$slug);
		return $find->first();
	}
	public function search($key)
	{
		return $this->like('judulBerita',$key);
	}
}
