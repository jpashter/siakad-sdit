<?php

namespace App\Models;

use CodeIgniter\Model;

class TentangModels extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'tentang';
	protected $primaryKey           = 'idTentang';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDeletes       = false;
	protected $protectFields        = true;
	protected $allowedFields        = ['tentang','keterangan','isiTentang','slug'];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';
}
